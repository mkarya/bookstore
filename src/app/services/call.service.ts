import { Injectable } from '@angular/core';
import { Http, ResponseOptions, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { JwtService } from '../jwt.service'; 

@Injectable()
export class CallService {

  constructor(private http: Http, private token: JwtService, private response: ResponseOptions) { }

  featchData(){
     return this.http.get("/api", this.token.jwtToken())
     .map(res => res.json())
  }

  newArrivales(){
     return this.http.get("/api/newarrivales")
     .map(res => res.json())
  }
  

}
