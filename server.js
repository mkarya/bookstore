var express = require('express'),
    path    = require('path'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    mysql = require('mysql'),
    configDoc = require('./config/config'),
    flash = require('connect-flash'),
    jwt = require('jsonwebtoken'),
    app = express();



    // Mysql Connection
var connection = mysql.createConnection(configDoc.mysqlDetails);
    connection.connect(function(err){
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }
    console.log('connected as id ' + connection.threadId);
    });


    // session
 
    var session = require('express-session');
    app.use(session(configDoc.expressSession));
    
    // Authentication
    var passport =  require('passport'); 

        

    var localStrategy = require('passport-local').Strategy;
    
    require('./config/passport')(passport, localStrategy, connection);
    
    
// set static folder 
app.use(express.static(path.join(__dirname, 'dist')));

// body parser

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

var rootPath = path.join(__dirname, 'dist'); 


 
// catch all api
require('./routes/userRoutes')(app, connection, passport, jwt); // userlogin
require('./routes/productroute')(app, connection);
 

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});



app.listen('3000', function(){
    console.log('app started on port 3000');
})

