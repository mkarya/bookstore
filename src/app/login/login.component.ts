import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CallService } from '../services/call.service';
import { UserloginService } from '../services/userlogin.service';
import { Subject } from 'rxjs/Subject'
import {MdDialog, MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  myform;
  http;
  message: string;

    posts = [];
  public checkout = new Subject<any>();
  constructor(private myht: CallService, private router: Router, private UserloginService: UserloginService, public dialogRef: MdDialogRef<LoginComponent>)   { }


  ngOnInit() {
    this.myform = new FormGroup({
      email : new FormControl(""),
      password: new FormControl("")
    });

     
  };

closedilog(){
  this.dialogRef.close();
}
   login(data){

    this.dialogRef.close();
     this.UserloginService.login(data).subscribe(
      (data) => {  
        if(data.success){
          this.UserloginService.setToken(data.token);
          this.checkout.next({a: "hellow"});
          this.router.navigate(["/dashboard"]);
           
        }else{
          this.message = data.message;
        }
         
        
      },
      (error) =>{
        this.message = error
      }
    );
   }

   
 



}
