import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { JwtService } from './jwt.service';


@Injectable()
export class ProductService {

  constructor(private http: Http, private token: JwtService) { }


  newArrivales(){
     return this.http.get("/api/newarrivales")
     .map(res => res.json())
  }

  getProducts(){
     return this.http.get("/api/getproducts")
     .map(res => res.json())
  }

  getSingleProduct(){
     return this.http.get("/api/singleproduct/:id")
     .map(res => res.json())
  }
  




}
