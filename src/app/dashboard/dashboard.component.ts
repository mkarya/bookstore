import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute, Router, Data } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  mydata= [];
  constructor(private route: ActivatedRoute, private router : Router) { }

  ngOnInit() {
    this.route.data.subscribe( (data: Data) => this.mydata = data.resolve )
  }

}
