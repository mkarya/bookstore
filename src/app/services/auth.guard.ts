import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserloginService } from './userlogin.service';


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private userDetails: UserloginService, private route: Router  ){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {       
    if(this.userDetails.isUserLogin()){
      return this.userDetails.isUserLogin(); 
    }else{
      this.route.navigate(['/']);
    }
      


  }
}
