import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-productplaceholder',
  templateUrl: './productplaceholder.component.html',
  styleUrls: ['./productplaceholder.component.css'],
  inputs: ['productData', 'tempType' ]
})
export class ProductplaceholderComponent implements OnInit {

  @Output() childmsg: EventEmitter<any> = new EventEmitter();

  private productData: any[];
  private tempType: string; 

  
  constructor() { }

  ngOnInit() {
       
  }

  sendData(ag){
    this.childmsg.emit({data: "you have selected"+ag})
  }

}
