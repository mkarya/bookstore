import { Component, OnInit } from '@angular/core';
import { UserloginService } from '../services/userlogin.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  myform;
  postData;
  constructor(private user: UserloginService) { }

  ngOnInit() {
    this.myform = new FormGroup({
      email : new FormControl(""),
      password : new FormControl("")
    })
  }

  
  singup(data){
   
    this.user.signup(data).subscribe(
    data => this.postData = data.success,
    error => alert(error),
    () => console.log("finish")

    );
  }
}
