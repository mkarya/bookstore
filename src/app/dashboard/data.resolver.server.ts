import { Injectable } from '@angular/core';
import { CanActivate, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CallService } from '../services/call.service';

interface Data{
    book_id: string;
    book_title: string;
    book_discription: string;
    price: number;
}
@Injectable()
export class DataResolver implements Resolve<Data> {

  constructor(private data: CallService ){}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<Data> | Promise<Data> | Data {       
    return this.data.featchData();

  }
}
