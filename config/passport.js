var hasingPass = require('./passhashing');

module.exports = function(passport, localStrategy, connection){

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
		done(null, user.user_id);
    });
 
    // used to deserialize the user
    passport.deserializeUser(function(id, done) {     	 
		connection.query("select user_id from users where user_id = "+id, function(err,rows){	
			done(err, rows[0]);
		});
    });


    // signup localStrategy
    
    passport.use("signup", new localStrategy({
        usernameField: 'email',  // passport only accept username / password but if filed name changed then use this
       passwordField: 'password',
       passReqToCallback: true  // special to get req param in callback
    },
    // callback 
    function(req, email, password, done){
        
        connection.query("SELECT email FROM users WHERE email ='"+email+"' ",  function(err, data){
            if(err){return done(err);}
            if(data.length){ return done(null, false,  req.flash('error', "email is already in use"));}
            
            	var hashedPassword = hasingPass.encryptPassword(password),
                     activationKey = hasingPass.encryptPassword(email),
                     insertQuery = 'INSERT INTO users (email, password, activation_key) VALUES ("'+email+'", "'+hashedPassword+'", "'+activationKey+'")';
 
                     connection.query(insertQuery, function(error, rows){
                    if(error){throw error};
                    return done(null, rows.insertId, req.flash('success', "account is created"));
                 });

        })
    }
    ));




    // LOGIN STRATEGY
    
    passport.use("login", new localStrategy({
        usernameField: 'email',  // passport only accept username / password but if filed name changed then use this
       passwordField: 'password',
       passReqToCallback: true  // special to get req param in callback
    },
    // callback 
    function(req, email, password, done){
       
        connection.query("SELECT user_id, email, password, status FROM users WHERE email ='"+email+"' ",  function(err, data){
         
            if(err){return done(err);}
            if(!data.length){ return done(null, false,  req.flash('error', "No account found with that email address."));}

            
            if(!hasingPass.validPassword(password, data[0])){   
                    
                return done(null, false,  req.flash('error', "Wrong credential."));
            }
                
            
            return done(null, data[0], req.flash('success', "Logged in successfully."))

        })
    }
    ));

}