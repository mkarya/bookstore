import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MdButtonModule, MdCheckboxModule, MdCardModule, MdMenuModule, MdToolbarModule, MdDialogModule, MdIconModule, MdInputModule} from '@angular/material';
 
import { RouterModule, Routes } from '@angular/router';
import {  ReactiveFormsModule, FormsModule } from '@angular/forms'



import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CallService } from './services/call.service';
import { DashboardComponent } from './dashboard/dashboard.component';




import { UserloginService } from './services/userlogin.service';
import { DataResolver } from './dashboard/data.resolver.server'
import { AuthGuard } from './services/auth.guard';
import { SignupComponent } from './signup/signup.component';
import { JwtService } from './jwt.service';
import { SliderComponent } from './slider/slider.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ProductplaceholderComponent } from './productplaceholder/productplaceholder.component';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { SlideDirective } from './slide.directive';
import { ProductpageComponent } from './productpage/productpage.component';

import { ProductService } from './product.service';
import { SidewidgetComponent } from './sidewidget/sidewidget.component';
import { TestComponent } from './test/test.component';



 const route: Routes = [
  {path: "", component: HomepageComponent},

  
  {path: "products", component: ProductpageComponent},
  {path:"products/:productname", component: NewsletterComponent},
  {path: "product-category", component: ProductpageComponent,  
      children: [
            {path:":catname", component: TestComponent},
            {path: ':catname/:subcate', component: SliderComponent}
  ]},

  {path: "dashboard", component: DashboardComponent, canActivate: [AuthGuard], resolve: {resolve: DataResolver}}
    
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    SignupComponent,
    SliderComponent,
    HomepageComponent,
    ProductplaceholderComponent,
    NewsletterComponent,
    SlideDirective,
    ProductpageComponent,
    SidewidgetComponent,
    TestComponent 
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(route),
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MdButtonModule,
    MdCheckboxModule,
    MdCardModule,
    MdMenuModule,
    MdToolbarModule,
    MdIconModule,
    MdDialogModule,
    MdInputModule
    
    
    
  ],
  entryComponents: [ 
    LoginComponent
  ],
  providers: [CallService, UserloginService, AuthGuard, DataResolver, JwtService, ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
