module.exports = function(app, connection){
 
    var products = require('../models/products')(connection);

    app.get("/api/newarrivales", products.newarrivals );  
    
    return app;
}