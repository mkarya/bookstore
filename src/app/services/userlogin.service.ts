import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/do';




@Injectable()
export class UserloginService {

  loginStatus = new Subject<any>();
  constructor(private router: Router, private http: Http) { }
  
  isUserLogin(){
    let a = window.localStorage.getItem("token");      
   if(a){
     return true
   }else{
     return false
   }
  }
 
  login(data){
     return this.http.post("/api/login", data)
          .map(res => res.json())
          .do(
             (data)=>{
               if(data.success){
                 this.loginStatus.next(data.success);
               }
             }
          )
           
   }

   signup(data){ 
     return this.http.post("/api/signup", data)
          .map(res => res.json()) 
   }

   setToken(token){
    localStorage.setItem("token", token);
   }

  logout(){
    this.http.post("/api/singout", {}) 
    .map(res => res.json())
    .subscribe(
      (data)=>{
        if(data.success){
            this.loginStatus.next(false);
            window.localStorage.removeItem('token');
            this.router.navigate(['/']);
        }
      }
    )
    
  }

}
