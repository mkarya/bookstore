module.exports = function(connection, passport, jwt){
  

 var userMod = new Object();
    userMod.singUp =  function(req, res, next){ 
        passport.authenticate('signup', function(err, user, info){
            if(err){return next(err)} // will generate a 500 error

            if (!user) { // if any user not assigned to this
                return res.json({ success : false, message: req.flash('error') });
               }
            return res.json({ success : true, message : req.flash('success')});
        })(req, res, next);
    }

    userMod.login = function(req, res, next){
        
        passport.authenticate('login', function(err, user){
            if(err){ throw err};
            if(!user){
                return res.json({ success : false, message: req.flash('error') });
            }


            req.login(user, function(err) {
                 if (err) {   return next(err); }
                var token = jwt.sign({id: user.user_id, email: user.email, status: user.status}, 'Q3UBzdH9GEfiRCTKbi5MTPyChpzXLsTD25#2sd6*%!#52#@4Hoh@#', { expiresIn: 60 * 60 * 14});
			  	return res.json({ success : true, message : req.flash('success'),  token: token });

            });


        })(req, res, next)
    }
    

    userMod.singout = function(req, res, next){ 
        req.logout(); 
        res.json({success: true})
    }
    return userMod;
 
  
} 
