import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserloginService } from '../services/userlogin.service';

import {MdDialog, MdDialogRef} from '@angular/material';
import { LoginComponent } from '../login/login.component'


 @Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [MdDialog]
})
export class HeaderComponent implements OnInit {

  constructor(private router:Router, private user: UserloginService, public dialog: MdDialog) { }
login;
  ngOnInit() {
    this.login = this.user.isUserLogin();
    
    this.user.loginStatus.subscribe(     
      (data) => {
         this.login = data
        }
    ) 
  }
   

   logout(){ 
     
    this.user.logout();
    this.user.loginStatus.subscribe(
      data => this.login = data
    )
  };


  openDialog() {
    let dialogRef = this.dialog.open(LoginComponent);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
    });
  }


  
}


 