import { Injectable } from '@angular/core';
import { Headers, RequestOptions  } from '@angular/http';


@Injectable()
export class JwtService {

  constructor() { }

    jwtToken(){
      return new RequestOptions({
        headers: new Headers({
          'content-Type': 'application/json',
          'x-access-token' : localStorage.getItem('token')
        })
      });
  }
}
