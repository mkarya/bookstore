import { Component, OnInit } from '@angular/core';
import { CallService } from '../services/call.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {


  message: string;
  constructor(private serverData: CallService) { }

  arrivalData: any[];
  ngOnInit() {


    this.arrivalData =[
      {"product_id":1,"product_title":"Test product","product_description":"hi this is test product one","product_price":0,"feature_img":"http://i.ebayimg.com/00/s/NDAwWDMwMA==/z/k6AAAOxylpNTT8Jy/$_32.JPG","stock":0,"category":null,"posted_on":"2017-08-02T08:11:02.000Z"},
      {"product_id":4,"product_title":"test product 2","product_description":"this is test product  2","product_price":0,"feature_img":"https://s-media-cache-ak0.pinimg.com/736x/32/74/f9/3274f99ed2b33e5d449069cb269dd71f--nike-shoes-on-sale-nike-shoes-.jpg","stock":0,"category":null,"posted_on":"2017-08-02T08:11:02.000Z"},
      {"product_id":6,"product_title":"test product 3","product_description":"this is test product 3","product_price":0,"feature_img":"http://img.tennis-warehouse.com/watermark/rs.php?path=WWTDII-BK-1.jpg&nw=300","stock":0,"category":null,"posted_on":"2017-08-02T08:11:02.000Z"},
      {"product_id":8,"product_title":"a","product_description":null,"product_price":0,"feature_img":"http://cdn3.bigcommerce.com/s-233ct/products/120/images/882/JULIA_RougeFleur_Front__61346.1499743849.300.550.jpg?c=2","stock":0,"category":null,"posted_on":"2017-08-02T08:11:02.000Z"}
    ];

    // this.serverData.newArrivales().subscribe(
    //   (data) => { this.arrivalData = data.data }
    // )



  }

  getdata(recivedData){
    this.message = recivedData.data;
  }

}
