import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductplaceholderComponent } from './productplaceholder.component';

describe('ProductplaceholderComponent', () => {
  let component: ProductplaceholderComponent;
  let fixture: ComponentFixture<ProductplaceholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductplaceholderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductplaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
